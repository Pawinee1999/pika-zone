using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class enemyPatrolT1 : MonoBehaviour
{
    public GameObject pointE;
    public GameObject pointF;
    public Rigidbody2D rb;
    private Animator anim;
    private Transform currentPoint;
    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
       // anim = GetComponent<Animator>();
        currentPoint = pointF.transform;
       // anim.SetBool("isRunning", true);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 point = currentPoint.position - transform.position;
        if (currentPoint == pointE.transform)
        {
            rb.velocity = new Vector2(speed, 0);
        }
        else
        {
            rb.velocity = new Vector2(-speed, 0);
        }

        
        if (Vector2.Distance(transform.position, currentPoint.position) < 0.5f && currentPoint == pointF.transform)
        {
            currentPoint = pointE.transform;
        }
        
        if (Vector2.Distance(transform.position, currentPoint.position) < 0.5f && currentPoint == pointE.transform)
        {
            currentPoint = pointF.transform;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(pointE.transform.position, 0.5f);
        Gizmos.DrawWireSphere(pointF.transform.position, 0.5f);
        Gizmos.DrawLine(pointE.transform.position, pointF.transform.position);
    }
    
}
