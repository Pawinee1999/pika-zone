using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioWalk : MonoBehaviour
{
    public AudioSource walkSound;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) ||Input.GetKey(KeyCode.A) ||Input.GetKey(KeyCode.D) )
        {
            walkSound.enabled = true;
        }
        else
        {
            walkSound.enabled = false;
        }
        
    }
}
