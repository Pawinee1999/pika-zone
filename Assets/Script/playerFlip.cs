using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerFlip: MonoBehaviour
{
    
    public float speedP = 5f;
    public Rigidbody2D rbPlayer; 
    private Vector2 move;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    
    


   
    void Update()
    {
        
        move.x = Input.GetAxisRaw("Horizontal");
        move.y = Input.GetAxisRaw("Vertical");
        Vector3 characterScale = transform.localScale;
        
    }
    
    void FixedUpdate()
    {
        rbPlayer.MovePosition(rbPlayer.position + move * speedP * Time.fixedDeltaTime);
        
        if (move.x != 0)
        {
            _spriteRenderer.flipX = move.x < 0;
        }
        if (move.y != 0)
        {
            _spriteRenderer.flipX = move.y < 0;
        }
        
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.tag == "Enemy")
        {
            SceneManager.LoadScene("Exit");
        }
        
        if (collision.gameObject.tag == "Fire")
        {
            SceneManager.LoadScene("Exit");
        }
        
        if (collision.gameObject.tag == "river")
        {
            SceneManager.LoadScene("Exit");
        }
    }
    
}
