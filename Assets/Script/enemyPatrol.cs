using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class enemyPatrolT : MonoBehaviour
{
    public GameObject pointC;
    public GameObject pointD;
    public Rigidbody2D rb2;
    private Transform currentPoint;
    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        rb2 = GetComponent<Rigidbody2D>();
        currentPoint = pointD.transform;
       
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 point = currentPoint.position - transform.position;
        if (currentPoint == pointC.transform)
        {
            rb2.velocity = new Vector2(0, speed);
        }
        else
        {
            rb2.velocity = new Vector2(0, -speed);
        }

        
        if (Vector2.Distance(transform.position, currentPoint.position) < 0.5f && currentPoint == pointC.transform)
        {
            currentPoint = pointD.transform;
        }
        
        if (Vector2.Distance(transform.position, currentPoint.position) < 0.5f && currentPoint == pointD.transform)
        {
            currentPoint = pointC.transform;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(pointC.transform.position, 0.5f);
        Gizmos.DrawWireSphere(pointD.transform.position, 0.5f);
        Gizmos.DrawLine(pointC.transform.position, pointD.transform.position);
    }
    
}
