using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMove : MonoBehaviour
{
    public float speedPlat;
    public Transform plat1;
    public Transform plat2;
    private bool turnback;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x >= plat1.position.x)
        {
            turnback = true;
        }

        if (transform.position.x <= plat2.position.x)
        {
            turnback = false;
        }


        if (turnback)
        {
            transform.position = Vector2.MoveTowards(transform.position, plat2.position, speedPlat * Time.deltaTime);
        }
        else
        {
            transform.position = Vector2.MoveTowards(transform.position, plat1.position, speedPlat * Time.deltaTime);
        }
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(plat1.transform.position, 0.5f);
        Gizmos.DrawWireSphere(plat2.transform.position, 0.5f);
        Gizmos.DrawLine(plat1.transform.position, plat2.transform.position);
    }
}
