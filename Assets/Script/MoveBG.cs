using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBG : MonoBehaviour
{
    [Range(1f, -1f)] 
    public float moveSpeed = 2f;
    private float offset;
    private Material mat;
    
    void Start()
    {
        mat = GetComponent<Renderer>().material;
    }
    void Update()
    {

        offset += (Time.deltaTime * moveSpeed) / 10f;
        mat.SetTextureOffset("_MainTex", new Vector2(offset, 0));
    }
}