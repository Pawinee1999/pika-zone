using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonChang : MonoBehaviour
{
    
    public void onClickNextScene()
    {
        SceneManager.LoadScene("GameScene1");
    }

    public void onClickBack()
    {
        SceneManager.LoadScene("MainManuScene");
    }

    public void OnClickSetting()
    {
        
    }

    public void onClickExit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
    
    void Update()
    {
        
    }
}
