using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyPatrolT2 : MonoBehaviour
{
     public GameObject pointG;
        public GameObject pointH;
        public Rigidbody2D rb;
        private Transform currentPoint;
        public float speed;
        
        // Start is called before the first frame update
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            currentPoint = pointH.transform;
        }
    
        // Update is called once per frame
        void Update()
        {
            Vector2 point = currentPoint.position - transform.position;
            if (currentPoint == pointG.transform)
            {
                rb.velocity = new Vector2(speed, 0);
            }
            else
            {
                rb.velocity = new Vector2(-speed, 0);
            }
    
            
            if (Vector2.Distance(transform.position, currentPoint.position) < 0.5f && currentPoint == pointH.transform)
            {
                currentPoint = pointG.transform;
            }
            
            if (Vector2.Distance(transform.position, currentPoint.position) < 0.5f && currentPoint == pointG.transform)
            {
                currentPoint = pointH.transform;
            }
        }
    
        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(pointG.transform.position, 0.5f);
            Gizmos.DrawWireSphere(pointH.transform.position, 0.5f);
            Gizmos.DrawLine(pointG.transform.position, pointH.transform.position);
        }
}
